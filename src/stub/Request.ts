'use strict';

export class Request {

    constructor(public method:string, public path:string, public headers:{[key:string]:string}, public body: string) {

    }
}