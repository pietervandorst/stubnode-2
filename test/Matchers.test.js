'use strict';
const Matchers_1 = require('../src/matcher/Matchers');
const Stub_1 = require('../src/stub/Stub');
const Request_1 = require('../src/stub/Request');
const chai_1 = require('chai');
describe('Matchers test', () => {
    let sut;
    beforeEach(() => {
        sut = new Matchers_1.Matchers();
    });
    describe('matchHttpMethod', () => {
        it('should return true when sut attempts to match method: GET to GET', () => {
            let stub = new Stub_1.Stub(new Request_1.Request('GET', null, null), null);
            chai_1.expect(sut.matchHttpMethod(stub, 'GET')).to.equal(true);
        });
        it('should return false when sut attempts to match method: POST to GET', () => {
            let stub = new Stub_1.Stub(new Request_1.Request('GET', null, null), null);
            chai_1.expect(sut.matchHttpMethod(stub, 'POST')).to.equal(false);
        });
    });
    describe('matchPath', () => {
        it('should return true when sut attempts to match path: /test/route to /test/route', () => {
            let stub = new Stub_1.Stub(new Request_1.Request(null, '/test/route', null), null);
            chai_1.expect(sut.matchPath(stub, '/test/route')).to.equal(true);
        });
        it('should return false when sut attempts to match path: /test to /test/route', () => {
            let stub = new Stub_1.Stub(new Request_1.Request(null, '/test', null), null);
            chai_1.expect(sut.matchPath(stub, '/test/route')).to.equal(false);
        });
    });
    describe('matchHttpHeaders', () => {
        it('should return true when sut attempts to match header: "custom-header":"value" to "custom-header":"value"', () => {
            let stub = new Stub_1.Stub(new Request_1.Request(null, null, { 'custom-header': 'value' }), null);
            chai_1.expect(sut.matchHttpHeaders(stub, { 'custom-header': 'value' })).to.equal(true);
        });
        it('should return false when sut attempts to match header: "custom-header":"value" to "custom":"value"', () => {
            let stub = new Stub_1.Stub(new Request_1.Request(null, null, { 'custom': 'value' }), null);
            chai_1.expect(sut.matchHttpHeaders(stub, { 'custom-header': 'value' })).to.equal(false);
        });
    });
});
//# sourceMappingURL=Matchers.test.js.map